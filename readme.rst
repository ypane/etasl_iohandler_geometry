etasl_iohandler_geometry
========================

Introduction
------------

This package provides additional I/O-handlers for the etasl_rtt component.
The I/O-handlers are all related to ROS geometry messages.


You can load this component as follows (oro-script):

.. code-block:: lua

    import("etasl_iohandler_geometry")
    loadComponent("io","Etasl_IOHandler_Geometry")


This component is used as a *factory*.
It is not necessary to start this component or to associate an activity with this component.
Once the I/O-handler is created using one of this component's operations, the component is not
needed any more.


I/O-handlers
------------

.. code-block:: lua

     add_etaslvar_deriv_geometry_msgs_twist_inputport( 
        string const& etasl_comp_name, string const& etasl_comp_name, string const& portname, string const& portdocstring 
       ) : bool
       adds an IOHandler to an eTaSL component that gets derivative values from a port into eTaSL
       etasl_comp_name : name of the eTaSL component to add the port to
       etasl_comp_name : name of the eTaSL component to add the port to
       portname : name of the port
       portdocstring : documentation for the port

     add_etaslvar_geometry_msgs_pose_inputport( 
         string const& etasl_comp_name, string const& portname, string const& portdocstring, string const& varname, unknown_t const& defaultvalue 
       ) : bool
       adds an IOHandler to an eTaSL component that gets values from a port into eTaSL
       etasl_comp_name : name of the eTaSL component to add the port to
       portname : name of the port
       portdocstring : documentation for the port
       varname : variable describing the contents of the inputport
       defaultvalue : value describing the default value (can be empty)

     add_etaslvar_geometry_msgs_pose_outputport( 
        string const& etasl_comp_name, string const& etasl_comp_name, string const& portname, string const& portdocstring 
       ) : bool
       adds an IOHandler to an eTaSL component that sets values from eTaSL on a port
       etasl_comp_name : name of the eTaSL component to add the port to
       etasl_comp_name : name of the eTaSL component to add the port to
       portname : name of the port
       portdocstring : documentation for the port

     add_transform_broadcast( 
        string const& etasl_comp_name, strings const& parents, strings const& childs, strings const& varnames 
       ) : bool
       broadcast a frame from etasl to tf
       etasl_comp_name : name of the eTaSL component to add the port to
       parents : vector of the transforms' parent frame names
       childs : vector of the transforms' child frame names
       varnames : vector of the names of the etasl variable

     add_transform_lookup( 
         string const& etasl_comp_name, string const& parent, string const& child, string const& varname 
       ) : bool
       looks up a frame from tf and passes it to etasl
       etasl_comp_name : name of the eTaSL component to add the port to
       parent : tf's parent frame
       child : tf's child frame
       varname : name of the etasl variable


License
-------
GNU LGPL v3, see LICENSE


Author
------

Erwin Aertbeliën, 2016.

