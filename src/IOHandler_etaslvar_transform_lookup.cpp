#include "IOHandler_etaslvar_transform_lookup.hpp"
#include "quat.hpp"

namespace KDL {
    IOHandler_etaslvar_transform_lookup::IOHandler_etaslvar_transform_lookup(
                boost::shared_ptr<Context::Ptr>   _ctx,
                rtt_tf::TFInterface&              _tf_,
                const std::string&                _parent,
                const std::string&                _child,
                const std::string&                _varname
    ): 
        ctx(_ctx),
        tf_(_tf_),
        parent(_parent),
        child(_child),
        varname(_varname),
        ready(false),
        count(1)
        {}
        
    bool IOHandler_etaslvar_transform_lookup::configure_component() {
      return true;
    }

    bool IOHandler_etaslvar_transform_lookup::attach_to_etasl() {
        etaslvar = (*ctx)->getInputChannel<KDL::Frame>( varname );
        if (!etaslvar) {
            errormsg="(transform_lookup): eTaSL input variable '"+varname+"' does not exists or it is not of type Frame";
            return false;
        } 
        return true;
    }
    
    bool IOHandler_etaslvar_transform_lookup::initialize() {
        return true;
    }

    bool IOHandler_etaslvar_transform_lookup::verify() {
        ready =  tf_.ready();
        if (!ready) {
            errormsg="(transform_lookup): it should be connected to tf service in order to lookup tf transforms";
        }
        return ready;
    }

    bool IOHandler_etaslvar_transform_lookup::update() {
        if (!ready) return true;
        if (tf_.canTransform(parent,child)) {
            geometry_msgs::TransformStamped tform = tf_.lookupTransform(parent, child);
            etaslvar->setValue( 
                    Frame(
                      setQuaternion(
                            tform.transform.rotation.x,
                            tform.transform.rotation.y,
                            tform.transform.rotation.z,
                            tform.transform.rotation.w
                      ),
                      Vector(  tform.transform.translation.x,
                                    tform.transform.translation.y,
                                    tform.transform.translation.z 
                      )
                    ) 
            );
        } else {
            if (count==0) {
                RTT::log(RTT::Error)<<"could not lookup transform " << parent << "," << child << RTT::endlog();
            }
            count = (count+1) % 100;
        }
        return true;
    }

    void IOHandler_etaslvar_transform_lookup::finish() {
    }

    int IOHandler_etaslvar_transform_lookup::getPriorityLevel() {
        return 20;
    }
    bool IOHandler_etaslvar_transform_lookup::detach_from_etasl() {
        return true;
    }
    bool IOHandler_etaslvar_transform_lookup::cleanup_component() {
        return true;
    }
    IOHandler_etaslvar_transform_lookup::~IOHandler_etaslvar_transform_lookup() {
    }
    std::string IOHandler_etaslvar_transform_lookup::getName() {
        return varname;
    }

} // namespace KDL
