#ifndef EXPRESSIONGRAPH_IOHANDLER_ETASLVAR_TRANSFORM_LOOKUP_HPP
#define EXPRESSIONGRAPH_IOHANDLER_ETASLVAR_TRANSFORM_LOOKUP_HPP
#include <rtt/RTT.hpp>
#include "IOHandler.hpp"
#include <expressiongraph/context.hpp>
#include <string>
#include <boost/shared_ptr.hpp>
#include <rtt_tf/tf_interface.h>

namespace KDL{

    /**
     * looks up a frame transformation from tf and passes it to an
     * eTaSL input variable.
     *
     * The default value is specified at the eTaSL side.
     *
     * This handler requires connection to the tf service before 
     * initialize() is called.
     */
    class IOHandler_etaslvar_transform_lookup:
        public IOHandler {
            boost::shared_ptr<Context::Ptr>   ctx;
            rtt_tf::TFInterface&              tf_;
            std::string                       parent;
            std::string                       child;
            std::string                       varname;
            VariableType<Frame>::Ptr          etaslvar;
            bool                              ready;
            int                               count;
        public:
            IOHandler_etaslvar_transform_lookup(
                boost::shared_ptr<Context::Ptr>   _ctx,
                rtt_tf::TFInterface&              _tf_,
                const std::string&                _parent,
                const std::string&                _child,
                const std::string&                _varname
            );
            virtual bool initialize();
            virtual bool configure_component();
            virtual bool attach_to_etasl();
            virtual bool verify();
            virtual bool update();
            virtual void finish();
            virtual bool detach_from_etasl();
            virtual bool cleanup_component();
            virtual int getPriorityLevel();
            virtual ~IOHandler_etaslvar_transform_lookup();
            virtual std::string getName();
    };

}//namespace KDL

#endif

