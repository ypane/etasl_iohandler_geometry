#include "etasl_iohandler_geometry-component.hpp"
#include <rtt/Logger.hpp>
#include <rtt/Component.hpp>
#include "IOHandler_etaslvar_geometry_msgs_pose_inputport.hpp"
#include "IOHandler_etaslvar_deriv_geometry_msgs_twist_inputport.hpp"
#include "IOHandler_etaslvar_geometry_msgs_pose_outputport.hpp"
#include "IOHandler_etaslvar_transform_lookup.hpp"
#include "IOHandler_etaslvar_transform_broadcast.hpp"
#include "etasl_rtt-component.hpp"
#include <geometry_msgs/Pose.h>
using namespace KDL;



Etasl_IOHandler_Geometry::Etasl_IOHandler_Geometry(std::string const& name) : 
    TaskContext(name),
    tf_(this)
{
    this->addOperation("add_etaslvar_geometry_msgs_pose_inputport",&Etasl_IOHandler_Geometry::add_etaslvar_geometry_msgs_pose_inputport,this,RTT::OwnThread)
        .doc("adds an IOHandler to an eTaSL component that gets values from a port into eTaSL")
        .arg("etasl_comp_name","name of the eTaSL component to add the port to")
        .arg("portname","name of the port")
        .arg("portdocstring","documentation for the port")
        .arg("varname","variable describing the contents of the inputport")
        .arg("defaultvalue","value describing the default value (can be empty)");
    this->addOperation("add_etaslvar_deriv_geometry_msgs_twist_inputport",&Etasl_IOHandler_Geometry::add_etaslvar_deriv_geometry_msgs_twist_inputport,this,RTT::OwnThread)
        .arg("etasl_comp_name","name of the eTaSL component to add the port to")
        .doc("adds an IOHandler to an eTaSL component that gets derivative values from a port into eTaSL")
        .arg("etasl_comp_name","name of the eTaSL component to add the port to")
        .arg("portname","name of the port")
        .arg("portdocstring","documentation for the port")
        .arg("varname","variable name describing the contents of the inputport");
    this->addOperation("add_etaslvar_geometry_msgs_pose_outputport",&Etasl_IOHandler_Geometry::add_etaslvar_geometry_msgs_pose_outputport,this,RTT::OwnThread)
        .arg("etasl_comp_name","name of the eTaSL component to add the port to")
        .doc("adds an IOHandler to an eTaSL component that sets values from eTaSL on a port")
        .arg("etasl_comp_name","name of the eTaSL component to add the port to")
        .arg("portname","name of the port")
        .arg("portdocstring","documentation for the port")
        .arg("varname","variable describing the contents of the inputport");
    this->addOperation("add_transform_lookup",&Etasl_IOHandler_Geometry::add_transform_lookup,this,RTT::OwnThread)
        .doc("looks up a frame from tf and passes it to etasl")
        .arg("etasl_comp_name","name of the eTaSL component to add the port to")
        .arg("parent","tf's parent frame")
        .arg("child","tf's child frame")
        .arg("varname","name of the etasl variable");
    this->addOperation("add_transform_broadcast",&Etasl_IOHandler_Geometry::add_transform_broadcast,this,RTT::OwnThread)
        .doc("broadcast a frame from etasl to tf")
        .arg("etasl_comp_name","name of the eTaSL component to add the port to")
        .arg("parents","vector of the transforms' parent frame names")
        .arg("childs","vector of the transforms' child frame names")
        .arg("varnames","vector of the names of the etasl variable");
}
 
bool Etasl_IOHandler_Geometry::add_etaslvar_geometry_msgs_pose_inputport( 
    const std::string& etaslcompname,
    const std::string& portname, 
    const std::string& portdocstring, 
    const std::string& varname, 
    const geometry_msgs::Pose& default_value) {

    etasl_rtt* e = getComponent(etaslcompname);
    if (e==NULL) return false;
    IOHandler::Ptr h( new KDL::IOHandler_etaslvar_geometry_msgs_pose_inputport( e, e->cctx, varname, default_value,portname, portdocstring) );
    if (h->configure_component() ) {
        e->ihc->addHandler( h );
        return true;
    }
    handler_config_failed(portname,"add_etaslvar_deriv_geometry_msgs_twist_inputport");
    return false;
}

bool Etasl_IOHandler_Geometry::add_etaslvar_deriv_geometry_msgs_twist_inputport(
        const std::string& etaslcompname,
        const std::string& portname, 
        const std::string& portdocstring, 
        const std::string& varname) {
    etasl_rtt* e = getComponent(etaslcompname);
    if (e==NULL) return false;
    IOHandler::Ptr h( new KDL::IOHandler_etaslvar_deriv_geometry_msgs_twist_inputport( e, e->cctx, varname, portname, portdocstring) );
    if (h->configure_component() ) {
        e->ihc->addHandler( h );
        return true;
    }
    handler_config_failed(portname,"add_etaslvar_deriv_geometry_msgs_twist_inputport");
    return false;
}
    
bool Etasl_IOHandler_Geometry::add_etaslvar_geometry_msgs_pose_outputport( 
    const std::string& etaslcompname,
    const std::string& portname, 
    const std::string& portdocstring, 
    const std::string& varname) {
    etasl_rtt* e = getComponent(etaslcompname);
    if (e==NULL) return false;
    IOHandler::Ptr h( new KDL::IOHandler_etaslvar_geometry_msgs_pose_outputport( e, e->cctx, varname, portname, portdocstring) );
    if (h->configure_component() ) {
        e->ihc->addHandler( h );
        return true;
    }
    handler_config_failed(portname,"add_etaslvar_geometry_msgs_pose_outputport");
    return false;
}

bool Etasl_IOHandler_Geometry::configureHook(){
  return true;
}

bool Etasl_IOHandler_Geometry::startHook(){
  return true;
}

void Etasl_IOHandler_Geometry::updateHook(){
}

void Etasl_IOHandler_Geometry::stopHook() {
}

void Etasl_IOHandler_Geometry::cleanupHook() {
}

bool Etasl_IOHandler_Geometry::add_transform_lookup(
        const std::string& etaslcompname,
        const std::string& parent, 
        const std::string& child, 
        const std::string& varname
) {
    etasl_rtt* e = getComponent(etaslcompname);
    if (e==NULL) return false;
    IOHandler::Ptr h( new KDL::IOHandler_etaslvar_transform_lookup(e->cctx, tf_, parent, child, varname) );
    if (h->configure_component() ) {
        e->ihc->addHandler( h );
        if (e->getTaskState()==Running) {
            // for runtime addition of IO handlers
            if (!h->attach_to_etasl())
                return false;
            if (!h->verify())
                return false;
        }
        return true;
    }
    
    handler_config_failed("TF","add_transform_lookup");
    return false;
}

bool Etasl_IOHandler_Geometry::add_transform_broadcast(
        const std::string& etaslcompname,
        const std::vector<std::string>& parents,
        const std::vector<std::string>& childs,
        const std::vector<std::string>& varnames
) {
    etasl_rtt* e = getComponent(etaslcompname);
    if (e==NULL) return false;
    IOHandler::Ptr h( new KDL::IOHandler_etaslvar_transform_broadcast(e->cctx, tf_, parents, childs, varnames) );
    if (h->configure_component() ) {
        e->ohc->addHandler( h );
        return true;
    }
    handler_config_failed("TF","add_transform_broadcast");
    return false;
}



// internal routine that gets a pointer to the etasl_rtt component and performs a series of checks.
// returns 0 if error condition occurred.
etasl_rtt* Etasl_IOHandler_Geometry::getComponent( const std::string & etaslcomp) {
    TaskContext* tc = getPeer(etaslcomp);
    if (tc==0) {
        std::cout << "unknown peer : " << etaslcomp << std::endl;
        RTT::log(RTT::Error) << getName() << " : etasl component name '" << etaslcomp<< "' refers to an unknown peer"<<RTT::endlog();
        return 0;
    }
    if (!tc->ready()) {
        RTT::log(RTT::Error) << getName() << " : etasl component name '" << etaslcomp<< "' is not ready"<<RTT::endlog();
        return 0;
    }

    etasl_rtt* e=dynamic_cast< etasl_rtt* >(tc);
    if (e==0) {
        RTT::log(RTT::Error) << getName() << " : etasl component name does not refer to the correct type of component" << std::endl;
        return 0;
    }

//     if (e->getTaskState()!=PreOperational) {
//         RTT::log(RTT::Error) << getName() << " : this operation can only be used in the PreOperational state"<<RTT::endlog();
//         return 0;
//     }
    //if (!e->etaslread) {
    //    RTT::log(RTT::Error) << getName() << " : this operation can only be used when an etasl definition has been read"<<RTT::endlog();
    //    return 0;
    //} 
    return e;
}

Etasl_IOHandler_Geometry::~Etasl_IOHandler_Geometry() {
}

/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Etasl_IOHandler_Geometry)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(Etasl_IOHandler_Geometry)

