#include "IOHandler_etaslvar_geometry_msgs_pose_outputport.hpp"
#include <algorithm>
namespace KDL {
    IOHandler_etaslvar_geometry_msgs_pose_outputport::IOHandler_etaslvar_geometry_msgs_pose_outputport(
                    RTT::TaskContext*   _tc,
                    boost::shared_ptr<Context::Ptr>    _ctx,
                    const std::string& _varname,
                    const std::string&  _portname,
                    const std::string&  _portdocstring

            ):
        tc(_tc),
        ctx(_ctx),
        varname(_varname),
        portname(_portname),
        portdocstring(_portdocstring) {
    }

    bool IOHandler_etaslvar_geometry_msgs_pose_outputport::configure_component() {
      tc->ports()->addPort(portname,outPort).doc(portdocstring);
      value_kdl = KDL::Frame::Identity();
      tf::poseKDLToMsg(value_kdl, value);
      outPort.setDataSample( value );
      return true;
    }
    bool IOHandler_etaslvar_geometry_msgs_pose_outputport::attach_to_etasl() {
        outp = (*ctx)->getOutputExpression<KDL::Frame>(varname); 
        if (!outp) {
            errormsg=portname+" (geometry_msgs/Pose): eTaSL output variable '"+varname+"' does not exists";
            return false;
        } 
        return true;
    }
    bool IOHandler_etaslvar_geometry_msgs_pose_outputport::initialize() {
        return true;
    }

    bool IOHandler_etaslvar_geometry_msgs_pose_outputport::verify(){
        return true;
    }

    bool IOHandler_etaslvar_geometry_msgs_pose_outputport::update(){
        value_kdl = outp->value();
        tf::poseKDLToMsg(value_kdl, value); 
        outPort.write( value );
        return true;
    }
    void IOHandler_etaslvar_geometry_msgs_pose_outputport::finish(){
        outPort.write( value );
    }
    bool IOHandler_etaslvar_geometry_msgs_pose_outputport::detach_from_etasl() {
        return true;
    }
    int IOHandler_etaslvar_geometry_msgs_pose_outputport::getPriorityLevel(){
        return 20;
    }
    bool IOHandler_etaslvar_geometry_msgs_pose_outputport::cleanup_component(){
        return true;
    }
    IOHandler_etaslvar_geometry_msgs_pose_outputport::~IOHandler_etaslvar_geometry_msgs_pose_outputport(){
        tc->ports()->removePort(portname);
    }
    std::string IOHandler_etaslvar_geometry_msgs_pose_outputport::getName() {
        return varname;
    }


} // namespace KDL
