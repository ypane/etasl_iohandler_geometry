#ifndef EXPRESSIONGRAPH_IOHANDLER_ETASLVAR_TRANSFORM_BROADCAST_HPP
#define EXPRESSIONGRAPH_IOHANDLER_ETASLVAR_TRANSFORM_BROADCAST_HPP

#include <rtt/RTT.hpp>
#include "IOHandler.hpp"
#include <expressiongraph/context.hpp>
#include <string>
#include <boost/shared_ptr.hpp>
#include <rtt_tf/tf_interface.h>

namespace KDL{

    /**
     * gets the values of outputexpressions in eTaSL sends it to an Orocos output port.
     * The port will be an array of doubles and the joints will be in the order as given
     * by the list of variable names.  
     *
     * The variables not present in eTaSL will trigger an error.
     *
     */
    class IOHandler_etaslvar_transform_broadcast:
        public IOHandler {
            boost::shared_ptr<Context::Ptr>                ctx;
            rtt_tf::TFInterface&              tf_;
            std::vector<std::string>                       parents;
            std::vector<std::string>                       childs;
            std::vector<std::string>                       varnames;
            std::vector<geometry_msgs::TransformStamped>   tforms;
            std::vector< Expression<Frame>::Ptr >          outp_exprs;
            bool                                           ready;
    public:
            typedef boost::shared_ptr<IOHandler> Ptr;

            IOHandler_etaslvar_transform_broadcast(
                boost::shared_ptr<Context::Ptr>   _ctx,
                rtt_tf::TFInterface&              _tf_,
                const std::vector<std::string>&                _parents,
                const std::vector<std::string>&                _childs,
                const std::vector<std::string>&                _varnames
            );
            virtual bool initialize();
            virtual bool configure_component();
            virtual bool attach_to_etasl();
            virtual bool verify();
            virtual bool update();
            virtual void finish();
            virtual bool detach_from_etasl();
            virtual bool cleanup_component();
            virtual int getPriorityLevel();
            virtual ~IOHandler_etaslvar_transform_broadcast();
            virtual std::string getName();
    };

}//namespace KDL

#endif

