#include "IOHandler_etaslvar_geometry_msgs_pose_inputport.hpp"

namespace KDL {
    IOHandler_etaslvar_geometry_msgs_pose_inputport::IOHandler_etaslvar_geometry_msgs_pose_inputport(
                RTT::TaskContext*          _tc,
                boost::shared_ptr<Context::Ptr>  _ctx,
                const std::string&         _varname,
                const geometry_msgs::Pose&          _default_value,
                const std::string&         _portname,
                const std::string&         _portdocstring
    ): 
        tc(_tc),
        ctx(_ctx),
        varname(_varname),
        default_value(_default_value),
        portname(_portname),
        portdocstring(_portdocstring)
        {}

    bool IOHandler_etaslvar_geometry_msgs_pose_inputport::configure_component() {
        tc->ports()->addPort(portname,inPort).doc(portdocstring);
        value = default_value;
        return true;
    }
    bool IOHandler_etaslvar_geometry_msgs_pose_inputport::attach_to_etasl() {
        inps = (*ctx)->getInputChannel<KDL::Frame>(varname); 
        if (!inps) {
            errormsg=portname+" (geometry_msgs/Pose): eTaSL input variable '"+varname+"' does not exists";
            return false;
        } else {
            tf::poseMsgToKDL(default_value,default_value_kdl);
            inps->setValue( default_value_kdl );
        }
        return true;

    }
    bool IOHandler_etaslvar_geometry_msgs_pose_inputport::initialize() {
        return true;
    }
    bool IOHandler_etaslvar_geometry_msgs_pose_inputport::verify() {
        return true;
    }
    bool IOHandler_etaslvar_geometry_msgs_pose_inputport::update() {
        RTT::FlowStatus fs;
        // read values from port and set data
        geometry_msgs::Pose msg;
        KDL::Frame value;
        fs  = inPort.read( msg );
        if (fs==RTT::NewData) {
                tf::poseMsgToKDL(msg, value);
                inps->setValue(value);
        }
        return true;
    }

    void IOHandler_etaslvar_geometry_msgs_pose_inputport::finish() {
    }
    bool IOHandler_etaslvar_geometry_msgs_pose_inputport::detach_from_etasl() {
        return true;
    }
    int IOHandler_etaslvar_geometry_msgs_pose_inputport::getPriorityLevel() {
        return 20;
    }
    bool IOHandler_etaslvar_geometry_msgs_pose_inputport::cleanup_component() {
        tc->ports()->removePort(portname);
        return true;
    }
    IOHandler_etaslvar_geometry_msgs_pose_inputport::~IOHandler_etaslvar_geometry_msgs_pose_inputport() {
        tc->ports()->removePort(portname);
    }
    std::string IOHandler_etaslvar_geometry_msgs_pose_inputport::getName() {
        return varname;
    }
} // namespace KDL
