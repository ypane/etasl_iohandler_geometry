#include "IOHandler_etaslvar_transform_broadcast.hpp"
#include <algorithm>
#include "quat.hpp"

namespace KDL {
    IOHandler_etaslvar_transform_broadcast::IOHandler_etaslvar_transform_broadcast(
                boost::shared_ptr<Context::Ptr>   _ctx,
                rtt_tf::TFInterface&              _tf_,
                const std::vector<std::string>&                _parents,
                const std::vector<std::string>&                _childs,
                const std::vector<std::string>&                _varnames
    ): ctx(_ctx), 
       tf_(_tf_), 
       parents(_parents), 
       childs(_childs), 
       varnames(_varnames), 
       outp_exprs(_varnames.size()),
       ready(false) {}
       
    bool IOHandler_etaslvar_transform_broadcast::configure_component() {
        return true;
    }

    bool IOHandler_etaslvar_transform_broadcast::attach_to_etasl() {
        if ((parents.size()!=childs.size())||(parents.size()!=varnames.size())) {
                errormsg="(transform_broadcast): IOHandler_etaslvar_transform_broadcast:  parent, child and varname arguments should be string vectors of the same length";
                return false;
        }
        tforms.clear();
        for (size_t i=0;i<varnames.size();++i) {
            outp_exprs[i] = (*ctx)->getOutputExpression<Frame>(varnames[i]);
            if (!outp_exprs[i]) {
                    RTT::log(RTT::Error)
                        << "eTaSL output variable '"
                        << varnames[i]
                        << "' does not exists" 
                        << RTT::endlog();
                    // ...continue without outputting this variable...
            } else {
                geometry_msgs::TransformStamped m;
                m.header.frame_id=parents[i] ;
                m.child_frame_id=childs[i];
                tforms.push_back(m);
            }
        }
        return true;
    }
    bool IOHandler_etaslvar_transform_broadcast::initialize() {
        return true;
    }
    
    bool IOHandler_etaslvar_transform_broadcast::verify(){
        ready = tf_.ready();
        if (!ready) {
          errormsg="(transform_broadcast): TF Broadcaster component not connected or not ready";
        }
        return ready; 
    }

    bool IOHandler_etaslvar_transform_broadcast::update(){
        if (!ready) return true;
        std::vector<geometry_msgs::TransformStamped>::iterator   tform = tforms.begin();
        for (size_t i=0;i<outp_exprs.size();++i) {
            if (outp_exprs[i]) {
                Frame F=outp_exprs[i]->value();     
                double x,y,z,w;
                getQuaternion(F.M,x,y,z,w);
                // parent and child frame are filled in during initialization.
                tform->header.stamp             = ros::Time::now();
                tform->transform.translation.x = F.p.x();
                tform->transform.translation.y = F.p.y();
                tform->transform.translation.z = F.p.z();
                tform->transform.rotation.x    = x;
                tform->transform.rotation.y    = y;
                tform->transform.rotation.z    = z;
                tform->transform.rotation.w    = w;
                tform++;
            }
        }
        tf_.broadcastTransforms( tforms );
        return true;
    }

    void IOHandler_etaslvar_transform_broadcast::finish(){
    }
    bool IOHandler_etaslvar_transform_broadcast::detach_from_etasl(){
        return true;
    }

    int IOHandler_etaslvar_transform_broadcast::getPriorityLevel(){
        return 20;
    }
    
    bool IOHandler_etaslvar_transform_broadcast::cleanup_component(){
        return true;
    }

    IOHandler_etaslvar_transform_broadcast::~IOHandler_etaslvar_transform_broadcast(){
    }
    std::string IOHandler_etaslvar_transform_broadcast::getName() {
        return varnames[0];
    }
} // namespace KDL
