#include "IOHandler_etaslvar_deriv_geometry_msgs_twist_inputport.hpp"

namespace KDL {
    IOHandler_etaslvar_deriv_geometry_msgs_twist_inputport::IOHandler_etaslvar_deriv_geometry_msgs_twist_inputport(
                RTT::TaskContext*          _tc,
                boost::shared_ptr<Context::Ptr> _ctx,
                const std::string&         _varname,
                const std::string&         _portname,
                const std::string&         _portdocstring
    ): 
        tc(_tc),
        ctx(_ctx),
        varname(_varname),
        default_value_kdl(KDL::Twist::Zero()),
        portname(_portname),
        portdocstring(_portdocstring)
        {}

    bool IOHandler_etaslvar_deriv_geometry_msgs_twist_inputport::configure_component() {
        tc->ports()->addPort(portname,inPort).doc(portdocstring);
        tf::twistKDLToMsg(default_value_kdl,value);
        return true;
    }
    bool IOHandler_etaslvar_deriv_geometry_msgs_twist_inputport::attach_to_etasl() {
        time_ndx = (*ctx)->getScalarNdx("time");
        inps = (*ctx)->getInputChannel<KDL::Frame>(varname); 
        if (!inps) {
            errormsg=portname+" (geometry_msgs/Twist): eTaSL input variable '"+varname+"' does not exists";
            return false;
        } else {
            inps->setJacobian(time_ndx, default_value_kdl );
        }
        return true;

    }
    bool IOHandler_etaslvar_deriv_geometry_msgs_twist_inputport::initialize() {
        return true;
    }
    bool IOHandler_etaslvar_deriv_geometry_msgs_twist_inputport::verify() {
        return true;
    }
    bool IOHandler_etaslvar_deriv_geometry_msgs_twist_inputport::update() {
        RTT::FlowStatus fs;
        // read values from port and set data
        geometry_msgs::Twist value;
        fs  = inPort.read( value );
        if (fs==RTT::NewData) {
                tf::twistMsgToKDL(value, value_kdl);
                inps->setJacobian(time_ndx,value_kdl);
        }
        return true;
    }

    void IOHandler_etaslvar_deriv_geometry_msgs_twist_inputport::finish() {
    }
    bool IOHandler_etaslvar_deriv_geometry_msgs_twist_inputport::detach_from_etasl() {
        return true;
    }

    int IOHandler_etaslvar_deriv_geometry_msgs_twist_inputport::getPriorityLevel() {
        return 20;
    }
    bool IOHandler_etaslvar_deriv_geometry_msgs_twist_inputport::cleanup_component() {
        tc->ports()->removePort(portname);
        return true;
    }
    IOHandler_etaslvar_deriv_geometry_msgs_twist_inputport::~IOHandler_etaslvar_deriv_geometry_msgs_twist_inputport() {
        tc->ports()->removePort(portname);
    }
    std::string IOHandler_etaslvar_deriv_geometry_msgs_twist_inputport::getName() {
        return varname;
    }
} // namespace KDL
