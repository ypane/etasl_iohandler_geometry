#ifndef OROCOS_ETASL_IOHANDLER_GEOMETRY_COMPONENT_HPP
#define OROCOS_ETASL_IOHANDLER_GEOMETRY_COMPONENT_HPP

#include <rtt/RTT.hpp>
#include <geometry_msgs/Pose.h>
#include <rtt_tf/tf_interface.h>
#include "etasl_rtt-component.hpp"
#include "errormsg-component.hpp"

class Etasl_IOHandler_Geometry : public RTT::TaskContext{
    rtt_tf::TFInterface tf_;                   // to receive and broadcast tf transform
    inline void handler_config_failed(const std::string& vname, const std::string& iohname) {
          RTT::log(RTT::Error) << errormsg::gen_io_error(errormsg::ERROR_IO_CONFIG,errormsg::BOLDMAGENTA) << ": " << vname << " (@" << iohname << ") "  << RTT::endlog();
        }
public:
    Etasl_IOHandler_Geometry(std::string const& name);

    virtual bool add_etaslvar_geometry_msgs_pose_inputport( 
        const std::string& etaslcompname,
        const std::string& portname, 
        const std::string& portdocstring, 
        const std::string& varname, 
        const geometry_msgs::Pose& default_value);
     
    virtual bool add_etaslvar_deriv_geometry_msgs_twist_inputport(
            const std::string& etaslcompname,
            const std::string& portname, 
            const std::string& portdocstring, 
            const std::string& varname);
     
    virtual bool add_etaslvar_geometry_msgs_pose_outputport( 
        const std::string& etaslcompname,
        const std::string& portname, 
        const std::string& portdocstring, 
        const std::string& varname);

     /**
     * \warning requires connecting to tf service
     */
    virtual bool add_transform_lookup(
            const std::string& etaslcompname,
            const std::string& parent, 
            const std::string& child, 
            const std::string& varname
    );

    /**
     * \warning requires connecting to tf service
     * \warning it is recommended that you declare all tf frames using one call to this
     *  operation.  Each call to this operation will result in a separate Ros message 
     *  that will  be send to ROS Tf.
     */
    virtual bool add_transform_broadcast(
            const std::string& etaslcompname,
            const std::vector<std::string>& parents, 
            const std::vector<std::string>& childs, 
            const std::vector<std::string>& varnames
    );

    virtual etasl_rtt* getComponent( const std::string & etaslcomp);

    virtual bool configureHook();
    virtual bool startHook();
    virtual void updateHook();
    virtual void stopHook();
    virtual void cleanupHook();
    virtual ~Etasl_IOHandler_Geometry();
};
#endif
