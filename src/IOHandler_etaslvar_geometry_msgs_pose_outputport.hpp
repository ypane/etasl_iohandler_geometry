#ifndef EXPRESSIONGRAPH_IOHANDLER_ETASLVAR_GEOMETRY_MSGS_POSE_OUTPUTPORT_HPP
#define EXPRESSIONGRAPH_IOHANDLER_ETASLVAR_GEOMETRY_MSGS_POSE_OUTPUTPORT_HPP

#include <rtt/RTT.hpp>
#include "IOHandler.hpp"
#include <expressiongraph/context.hpp>
#include <string>
#include <boost/shared_ptr.hpp>
#include <geometry_msgs/Pose.h>
#include <tf_conversions/tf_kdl.h>

namespace KDL{

    /**
     * gets the values of outputexpressions in eTaSL sends it to an Orocos output port.
     * The port will be an array of doubles and the joints will be in the order as given
     * by the list of variable names.  
     *
     * The variables not present in eTaSL will trigger an error.
     *
     */
    class IOHandler_etaslvar_geometry_msgs_pose_outputport:
        public IOHandler {
            RTT::TaskContext*          tc;
            RTT::OutputPort<geometry_msgs::Pose > outPort;
            Expression<KDL::Frame>::Ptr outp; 
            boost::shared_ptr<Context::Ptr> ctx;
            std::string   varname;
            std::string                portname;
            std::string                portdocstring;
            geometry_msgs::Pose        value;
            KDL::Frame                 value_kdl;
    public:
            typedef boost::shared_ptr<IOHandler> Ptr;

            IOHandler_etaslvar_geometry_msgs_pose_outputport(
                    RTT::TaskContext*   _tc,
                    boost::shared_ptr<Context::Ptr> _ctx,
                    const std::string& _varname,
                    const std::string&  _portname,
                    const std::string&  _portdocstring
            );
            virtual bool initialize();
            virtual bool configure_component();
            virtual bool attach_to_etasl();
            virtual bool verify();
            virtual bool update();
            virtual void finish();
            virtual bool detach_from_etasl();
            virtual bool cleanup_component();
            virtual int getPriorityLevel();
            virtual ~IOHandler_etaslvar_geometry_msgs_pose_outputport();
            virtual std::string getName();
    };

}//namespace KDL

#endif

